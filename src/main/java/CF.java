import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.util.Arrays;
import java.util.Scanner;

public class CF {

    private int N, M, Q;
    private Integer[][] userItemMatrix;
    private Double[][] userItemMatrixAverageByUser;
    private Double[][] userItemMatrixAverageByItem;
    private Double[][] userSimilarities;
    private Double[][] itemSimilarities;
    private Integer[][] queries;

    private void readInput() {
        Scanner sc = new Scanner(System.in);
        String[] firstLineChunks = sc.nextLine().split("\\s+");
        N = Integer.parseInt(firstLineChunks[0]);
        M = Integer.parseInt(firstLineChunks[1]);

        userItemMatrix = new Integer[N][];
        String[] lineChunks;
        for (int i = 0; i < N; i++) {
            lineChunks = sc.nextLine().split("\\s+");
            Integer[] row = new Integer[M];
            for (int j = 0; j < M; j++) {
                if (lineChunks[j].equals("X")) {
                    row[j] = null;
                } else {
                    row[j] = Integer.parseInt(lineChunks[j]);
                }
            }
            userItemMatrix[i] = row;
        }

        String[] qLineChunks = sc.nextLine().split("\\s+");
        Q = Integer.parseInt(qLineChunks[0]);

        queries = new Integer[Q][];
        for (int i = 0; i < Q; i++) {
            lineChunks = sc.nextLine().split("\\s+");
            Integer[] row = new Integer[4];
            for (int j = 0; j < 4; j++) {
                row[j] = Integer.parseInt(lineChunks[j]);
            }
            queries[i] = row;
        }

        normalizeByItem();
        normalizeByUser();
        calculateItemSimilarities();
        calculateUserSimilarities();
    }

    private void calculateUserSimilarities() {
        userSimilarities = new Double[M][M];
        Double[] array1 = new Double[N];
        Double[] array2 = new Double[N];
        for (int i = 0; i < M; i++) {
            for (int j = 0; j < M; j++) {
                if (i != j) {
                    for (int k = 0; k < N; k++) {
                        array1[k] = userItemMatrixAverageByUser[k][i];
                        array2[k] = userItemMatrixAverageByUser[k][j];
                    }
                    userSimilarities[i][j] = cosineSimilarity(array1, array2, N);
                } else {
                    userSimilarities[i][j] = 0.;
                }
            }
        }
    }

    private void calculateItemSimilarities() {
        itemSimilarities = new Double[N][N];
        for (int i = 0; i < N; i++) {
            for (int j = 0; j < N; j++) {
                if (i != j) {
                    double sim = cosineSimilarity(userItemMatrixAverageByItem[i], userItemMatrixAverageByItem[j], M);
                    itemSimilarities[i][j] = sim >= 0 ? sim : 0.;
                } else {
                    itemSimilarities[i][j] = 0.;
                }
            }
        }
    }

    private void normalizeByItem() {
        userItemMatrixAverageByItem = new Double[N][M];
        for (int i = 0; i < N; i++) {
            double rowSum = 0;
            int counter = 0;
            for (int j = 0; j < M; j++) {
                if (userItemMatrix[i][j] != null) {
                    rowSum += userItemMatrix[i][j];
                    counter++;
                }
            }
            rowSum /= counter;
            for (int j = 0; j < M; j++) {
                if (userItemMatrix[i][j] != null) {
                    userItemMatrixAverageByItem[i][j] = userItemMatrix[i][j] - rowSum;
                } else {
                    userItemMatrixAverageByItem[i][j] = null;
                }
            }
        }
    }

    private void normalizeByUser() {
        userItemMatrixAverageByUser = new Double[N][M];
        for (int j = 0; j < M; j++) {
            double columnSum = 0;
            int counter = 0;
            for (int i = 0; i < N; i++) {
                if (userItemMatrix[i][j] != null) {
                    columnSum += userItemMatrix[i][j];
                    counter++;
                }
            }
            columnSum /= counter;
            for (int i = 0; i < N; i++) {
                if (userItemMatrix[i][j] != null) {
                    userItemMatrixAverageByUser[i][j] = userItemMatrix[i][j] - columnSum;
                } else {
                    userItemMatrixAverageByUser[i][j] = null;
                }
            }
        }
    }

    private double cosineSimilarity(Double[] array1, Double[] array2, int len) {
        double sum1 = 0.;
        double sum2 = 0.;
        double sum3 = 0.;
        for (int i = 0; i < len; i++) {
            if (array1[i] != null && array2[i] != null) {
                sum1 += array1[i] * array2[i];
            }
            if (array1[i] != null) {
                sum2 += array1[i] * array1[i];
            }
            if (array2[i] != null) {
                sum3 += array2[i] * array2[i];
            }
        }
        return sum1 / Math.sqrt(sum2 * sum3);
    }

    private void printMatrix(Double[][] matrix, int rows, int cols) {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < rows; i++) {
            for (int j = 0; j < cols; j++) {
                if (matrix[i][j] != null) {
                    sb.append(matrix[i][j]).append(" ");
                } else {
                    sb.append("X").append(" ");
                }
            }
            sb.append("\n");
        }
        System.out.println(sb);
    }

    private double predictRatingByItem(int item, int user, int k) {
        double sum1 = 0.;
        double sum2 = 0.;
        Double[] itemSims = new Double[N];
        System.arraycopy(itemSimilarities[item], 0, itemSims, 0, N);
        for (; k > 0; k--) {
            Double max = Arrays.stream(itemSims).max(Double::compare).get();
            if (max == 0) {
                break;
            }
            int index = Arrays.asList(itemSims).indexOf(max);
            itemSims[index] = 0.;
            if (userItemMatrix[index][user] != null) {
                sum1 += userItemMatrix[index][user] * max;
                sum2 += max;
            } else {
                k++;
            }
        }
        return sum1 / sum2;
    }

    private double predictRatingByUser(int item, int user, int k) {
        double sum1 = 0.;
        double sum2 = 0.;
        Double[] userSims = new Double[M];
        System.arraycopy(userSimilarities[user], 0, userSims, 0, M);
        for (; k > 0; k--) {
            Double max = Arrays.stream(userSims).max(Double::compare).get();
            int index = Arrays.asList(userSims).indexOf(max);
            userSims[index] = 0.;
            if (userItemMatrix[item][index] != null) {
                sum1 += userItemMatrix[item][index] * max;
                sum2 += max;
            } else {
                k++;
            }
        }
        return sum1 / sum2;
    }

    public static void main(String[] args) {
        CF cf = new CF();
        cf.readInput();
        for (Integer[] query : cf.queries) {
            int item = query[0] - 1;
            int user = query[1] - 1;
            int mode = query[2];
            int k = query[3];
            double rating = mode == 0 ? cf.predictRatingByItem(item, user, k) : cf.predictRatingByUser(item, user, k);
            System.out.println(roundAndToString(rating));
        }
    }

    private static String roundAndToString(double num) {
        DecimalFormat df = new DecimalFormat("#.000");
        BigDecimal bd = new BigDecimal(num);
        BigDecimal res = bd.setScale(3, RoundingMode.HALF_UP);
        return df.format(res);
    }
}


